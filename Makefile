OBJS = main.o system.o block.o font.o game.o tile.o
LDFLAGS = -lSDL2 -lGL -lGLU

tetris : $(OBJS)
	g++ -o tetris $(OBJS) $(LDFLAGS)

main.o : system.h
system.o : system.h game.h font.h
tile.o : tile.h
game.o : system.h block.h
font.o :
block.o : block.h tile.h

clean :
	rm -v tetris $(OBJS)
