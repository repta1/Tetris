#include <array>
#include "game.h"
#include "tile.h"

#ifndef BLOCK_H
#define BLOCK_H

typedef enum {
	BLOCK_OK = 0,
	BLOCK_OUT_OF_BOUNDS,
	BLOCK_HITS_OCCUPIED_TILE,
	BLOCK_CANT_SPAWN
} block_err;

class Block {
public:
	Block( Game *game ) : game( game ) {}
	block_err place();
	void pickup();
	virtual void rotateleft() = 0;
	virtual void rotateright() = 0;
	virtual void draw(colorsetid_t colorset) = 0;
	virtual block_err move( int x, int y );
	void print_err( block_err err );

protected:
	Game *game;
	std::array<loc_t,4> squares;
	colorid_t colorid;
};

Block *newBlock( Game *game );

class OBlock : public Block {
public:
	OBlock( Game *game );
	virtual void rotateleft() {}
	virtual void rotateright() {}
	virtual void draw( colorsetid_t colorset );
};

class JBlock : public Block {
public:
	JBlock( Game *game );
	virtual void rotateleft();
	virtual void rotateright();
	virtual void draw( colorsetid_t colorset );
private:
	unsigned char rotation;
};

class LBlock : public Block {
public:
	LBlock( Game *game );
	virtual void rotateleft();
	virtual void rotateright();
	virtual void draw( colorsetid_t colorset );
private:
	unsigned char rotation;
};

class TBlock : public Block {
public:
	TBlock( Game *game );
	virtual void rotateleft();
	virtual void rotateright();
	virtual void draw( colorsetid_t colorset );
private:
	unsigned char rotation;
};

class SBlock : public Block {
public:
	SBlock( Game *game );
	virtual void rotateleft();
	virtual void rotateright() { rotateleft(); }
	virtual void draw( colorsetid_t colorset );
private:
	unsigned char rotation;
};

class ZBlock : public Block {
public:
	ZBlock( Game *game );
	virtual void rotateleft();
	virtual void rotateright() { rotateleft(); }
	virtual void draw( colorsetid_t colorset );
private:
	unsigned char rotation;
};

class IBlock : public Block {
public:
	IBlock( Game *game );
	virtual void rotateleft();
	virtual void rotateright() { rotateleft(); }
	virtual void draw( colorsetid_t colorset );
private:
	unsigned char rotation;
};
#endif // BLOCK_H