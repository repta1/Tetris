#include <iostream>
#include "SDL2/SDL.h"
#include "GL/glu.h"
#include "system.h"
#include "game.h"
#include "font.h"

System::System() {
	err = SYS_OK;
	running = true;

	keyboard.downKey = false;
	keyboard.upKey = false;
	keyboard.leftKey = false;
	keyboard.rightKey = false;

	std::cout << "Initializing SDL..." << std::endl;
	if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_EVENTS ) != 0 ) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		err = SYS_ERR_INIT_SDL;
		return;
	}

	std::cout << "Initializing window..." << std::endl;
	win = SDL_CreateWindow( "Tetris", 100, 100, 640, 480, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE );
	if ( win == NULL ) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		err = SYS_ERR_CREAT_WIN;
		return;
	}

	std::cout << "Initializing GL Context..." << std::endl;
	glcon = SDL_GL_CreateContext( win );
	if ( glcon == NULL ) {
		std::cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << std::endl;
		err = SYS_ERR_CREAT_GLCON;
		return;
	}

	glViewport( 0, 0, 640, 480 );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluOrtho2D( 0, 320, 240, 0 );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glShadeModel( GL_FLAT );

	fontInit();

	sdlTicks = 0;
	ticks = 0;

	game = new Game( this );
}

System::~System() {
	delete game;

	switch ( err ) {
	case SYS_OK:
		SDL_GL_DeleteContext( glcon );
	case SYS_ERR_CREAT_GLCON:
		SDL_DestroyWindow( win );
	default:
		SDL_Quit();
	}
}

void System::mainLoop() {
	running = true;
	Uint32 tickDeadline = SDL_GetTicks();
	SDL_Event evn;

	while ( running ) {
		tickDeadline = sdlTicks + 16;

		game->runtic();

		glClearColor( 0.0f, 0.0f, 0.0f, 1 );
		glClear( GL_COLOR_BUFFER_BIT );
		game->draw();
		SDL_GL_SwapWindow( win );
		
		checkGLError( "Frame end" );

		while ( !SDL_TICKS_PASSED( sdlTicks = SDL_GetTicks(), tickDeadline ) ) {
			if ( SDL_WaitEventTimeout( &evn, tickDeadline - sdlTicks ) ) {
				std::cout << "Got an event of type ";
				switch ( evn.type ) {
				case SDL_QUIT:
					std::cout << "QUIT" << std::endl;
					running = false;
					break;
				case SDL_WINDOWEVENT:
					std::cout << "WINDOWEVENT" << std::endl;
					doWindow( (SDL_WindowEvent *) &evn );
					break;
				case SDL_KEYDOWN:
				case SDL_KEYUP:
					std::cout << "KEYUP/DOWN" << std::endl;
					doInput( &evn );
				default:
					std::cout << "OTHER" << std::endl;
					;
				}
			}
		}

		ticks++;
	}
}

void System::printStr( const char *s ) {
	printString( s );
}

void System::checkGLError( const char *s ) {
	GLenum err = glGetError();
	if ( err != GL_NO_ERROR ) {
		std::cout << "GLError: " << s << std::endl;
		switch ( err ) {
		case GL_INVALID_ENUM:
			std::cout << "GL_INVALID_ENUM";
			break;
		case GL_INVALID_VALUE:
			std::cout << "GL_INVALID_VALUE";
			break;
		case GL_INVALID_OPERATION:
			std::cout << "GL_INVALID_OPERATION";
			break;
		case GL_STACK_OVERFLOW:
			std::cout << "GL_STACK_OVERFLOW";
			break;
		case GL_STACK_UNDERFLOW:
			std::cout << "GL_STACK_UNDERFLOW";
			break;
		case GL_OUT_OF_MEMORY:
			std::cout << "GL_OUT_OF_MEMORY";
			break;
		default:
			;
		}
		std::cout << std::endl;
	}
}

void System::quit() {
	running = false;
}

void System::doInput( SDL_Event *evn ) {
	if ( evn->type == SDL_KEYDOWN ) {
		switch ( evn->key.keysym.sym ) {
		case SDLK_RIGHT:
			if ( !keyboard.rightKey ) {
				keyboard.rightKey = true;
				game->doInput( evn );
			}
			break;
		case SDLK_LEFT:
			if ( !keyboard.leftKey ) {
				keyboard.leftKey = true;
				game->doInput( evn );
			}
			break;
		case SDLK_DOWN:
			if ( !keyboard.downKey ) {
				keyboard.downKey = true;
				game->doInput( evn );
			}
			break;
		default:
			game->doInput( evn );
		}
	}

	if ( evn->type == SDL_KEYUP ) {
		switch ( evn->key.keysym.sym ) {
		case SDLK_RIGHT:
			keyboard.rightKey = false;
			break;
		case SDLK_LEFT:
			keyboard.leftKey = false;
			break;
		case SDLK_DOWN:
			keyboard.downKey = false;
		default:
			;
		}
	}
}

void System::doWindow( SDL_WindowEvent *evn ) {
	if ( evn->event == SDL_WINDOWEVENT_RESIZED ) {
		glViewport( 0, 0, evn->data1, evn->data2 );
		double ratio = ( double )evn->data1 / ( double )evn->data2;
		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		if ( ratio >= ( 4.0 / 3.0 ) ) {
			double width = 240.0 * ratio;
			double offset = ( width - 320.0 ) / 2;
			gluOrtho2D( -offset, 320 + offset, 240, 0 );
		}
		else {
			double height = 320.0 / ratio;
			double offset = ( height - 240.0 ) / 2;
			gluOrtho2D( 0, 320, 240 + offset, -offset );
		}
		glMatrixMode( GL_MODELVIEW );
		glLoadIdentity();
	}
}
