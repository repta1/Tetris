#include <iostream>
#include <stdio.h>
#include "SDL2/SDL.h"
#include "GL/glu.h"
#include "system.h"
#include "block.h"

#define MIN(x, y) ((x) > (y) ? (y) : (x))
#define MAX(x, y) ((x) < (y) ? (y) : (x))

unsigned char levelSpeed[30] = { 48, 43, 38, 33, 28, 23, 18, 13,  8,  6,
								  5,  5,  5,  4,  4,  4,  3,  3,  3,  2,
								  2,  2,  2,  2,  2,  2,  2,  2,  2,  1 };

Game::Game(System *sys) : sys( sys ) {
	init();
}

void Game::runtic() {
	switch ( mode ) {
	case MODE_TITLE:
		titleTic();
		break;
	case MODE_SELECT:
		selectTic();
		break;
	case MODE_GAME:
		gameTic();
		break;
	default:
		;
	}
}

void Game::draw() {
	switch ( mode ) {
	case MODE_TITLE:
		titleDraw();
		break;
	case MODE_SELECT:
		selectDraw();
		break;
	case MODE_GAME:
		gameDraw();
		break;
	case MODE_PAUSE:
		pauseDraw();
		break;
	case MODE_OVER:
		overDraw();
		break;
	default:
		;
	}
}

void Game::doInput(SDL_Event *evn) {
	if ( evn->type == SDL_KEYDOWN ) {
		if ( evn->key.keysym.sym == SDLK_ESCAPE ) {
			sys->quit();
			return;
		}
	}

	switch ( mode ) {
	case MODE_TITLE:
		titleInput( evn );
		break;
	case MODE_SELECT:
		selectInput( evn );
		break;
	case MODE_GAME:
		gameInput(evn);
		break;
	case MODE_PAUSE:
		pauseInput( evn );
		break;
	case MODE_OVER:
		overInput( evn );
		break;
	default:
		;
	}
}

/****************
* BOARD FUNCTIONS
*****************/

bool Game::hasTile( loc_t loc ) {
	if ( loc.x >= BOARD_TILES_X || loc.y >= BOARD_TILES_Y
		|| loc.x < 0 || loc.y < 0 ) {
		return 0;
	}

	return board[loc.y][loc.x] != NULL;
}

int Game::placeTile( loc_t loc, tile_t tile ) {
	if ( board[loc.y][loc.x] == NULL ) {
		tile_t *newtile = new tile_t;
		newtile->colorid = tile.colorid;
		board[loc.y][loc.x] = newtile;
		return 0;
	}
	else {
		return 1;
	}
}

void Game::freeTile( loc_t loc ) {
	board[loc.y][loc.x] = NULL;
}

void Game::checkRows() {
	bool fullRow = true;
	unsigned char lines = 0;
	for ( int ity = 0; ity != BOARD_TILES_Y; ity++ ) {
		for ( int itx = 0; itx != BOARD_TILES_X; itx++ ) {
			if ( board[ity][itx] == NULL ) {
				fullRow = false;
			}
		}

		if ( fullRow ) {
			std::cout << "Full row detected" << std::endl;
			lines++;
			board[ity].fill( NULL );
			for ( int itr = ity; itr > 0; itr-- ) {
				board[itr].swap( board[itr - 1] );
			}
		}
		fullRow = true;
	}

	switch ( lines ) {
	case 1:
		state.score += 40 * ( state.level + 1 );
	case 2:
		state.score += 100 * ( state.level + 1 );
	case 3:
		state.score += 300 * ( state.level + 1 );
	case 4:
		state.score += 1200 * ( state.level + 1 );
	}
	
	state.lines += lines;
	if ( state.lines >= (10 * (state.level + 1)) ) {
		state.level++;
		colorset = state.level % 10;
		return;
	}
}

void Game::init() {
	mode = MODE_TITLE;
	colorset = 0;
	tic = 0;
	blockTic = 0;

	for ( auto it = board.begin(); it != board.end(); it++ ) {
		it->fill( NULL );
	}

	state.score = 0;
	state.lines = 0;
	state.level = 0;

	for ( auto it = state.blockcounts.begin(); it != state.blockcounts.end(); it++ ) {
		*it = 0;
	}

	currentBlock = NULL;
	nextBlock = NULL;
}

/****************
* TIC   FUNCTIONS
*****************/

void Game::titleTic() {
	return;
}

void Game::selectTic() {
	tic++;
}

void Game::gameTic() {
	block_err err = BLOCK_OK;
	tic++;
	blockTic++;

	if ( blockTic == levelSpeed[MIN(state.level, 29)] || ( sys->getKeys().downKey && !( blockTic % 2 ) ) ) {
		currentBlock->pickup();
		err = currentBlock->move( 0, 1 );
		currentBlock->print_err( err );
		currentBlock->place();
		blockTic = 0;
	}

	if ( err ) { // Block has hit the bottom
		delete currentBlock;
		checkRows();
		currentBlock = nextBlock;
		nextBlock = newBlock( this );

		if ( currentBlock->place() == BLOCK_CANT_SPAWN ) {
			mode = MODE_OVER;
		}
	}
}

/****************
* INPUT FUNCTIONS
*****************/

void Game::titleInput( SDL_Event *evn ) {
	if ( evn->type == SDL_KEYDOWN ) {
		if ( evn->key.keysym.sym == SDLK_RETURN ) {
			mode = MODE_SELECT;
		}
	}
}

void Game::selectInput( SDL_Event *evn ) {
	int *selection = ( int * )&state; // Repurpose the game state
	int col = *selection % 5;
	int row = *selection / 5;
	if ( evn->type == SDL_KEYDOWN ) {
		switch ( evn->key.keysym.sym ) {
		case SDLK_RIGHT:
			*selection = MIN( col + 1, 4 ) + 5 * row;
			break;
		case SDLK_LEFT:
			*selection = MAX( col - 1, 0 ) + 5 * row;
			break;
		case SDLK_DOWN:
			*selection = col + 5;
			break;
		case SDLK_UP:
			*selection = col;
			break;
		case SDLK_RETURN:
			int level = *selection;
			SDL_zero( state );
			state.level = level;
			colorset = level;
			currentBlock = newBlock( this );
			nextBlock = newBlock( this );
			currentBlock->place();
			mode = MODE_GAME;
		}
	}
}

void Game::gameInput( SDL_Event *evn ) {
	if ( evn->type == SDL_KEYDOWN ) {
		currentBlock->pickup();
		switch ( evn->key.keysym.sym ) {
		case SDLK_RIGHT:
			currentBlock->move( 1, 0 );
			break;
		case SDLK_LEFT:
			currentBlock->move( -1, 0 );
			break;
		case SDLK_z:
			currentBlock->rotateleft();
			break;
		case SDLK_x:
			currentBlock->rotateright();
			break;
		case SDLK_RETURN:
			mode = MODE_PAUSE;
		default:
			;
		}
		currentBlock->place();
	}
}

void Game::pauseInput( SDL_Event *evn ) {
	if ( evn->type == SDL_KEYDOWN ) {
		switch ( evn->key.keysym.sym ) {
		case SDLK_RETURN:
			mode = MODE_GAME;
			break;
		default:
			;
		}
	}
}

void Game::overInput( SDL_Event *evn ) {
	if ( evn->type == SDL_KEYDOWN ) {
		switch ( evn->key.keysym.sym ) {
		case SDLK_RETURN:
			init();
			break;
		default:
			;
		}
	}
}

/****************
* DRAW  FUNCTIONS
*****************/

void Game::titleDraw() {
	glPushMatrix();

	glTranslatef( 115.0, 80.0, 0.0 );
	sys->printStr( "It's Tetris." );
	glTranslatef( 0.0, 24.0, 0.0 );
	sys->printStr( "Press Enter." );

	glTranslatef( -50.0, 40.0, 0.0 );
	sys->printStr( "Z, X: Rotate Piece" );

	glTranslatef( 0.0, 16.0, 0.0 );
	sys->printStr( "Arrow Keys: Move Piece" );

	glPopMatrix();
}

void Game::selectDraw() {
	Uint32 *selection = ( Uint32 * )&state; // Repurpose the game state

	glPushMatrix();

	glTranslatef( 112.0, 96.0, 0.0 );
	sys->printStr( "Select Level" );

	glTranslatef( 12.0, 24.0, 0.0 );
	sys->printStr( "0 1 2 3 4" );
	glTranslatef( 0.0, 16.0, 0.0 );
	sys->printStr( "5 6 7 8 9" );

	glPopMatrix();

	glColor3f( 1.0, 0.5, 0.0 );
	int col = *selection % 5;
	int row = *selection / 5;
	if ( (tic % 4) >= 2 ) {
		glRectd( 124.0 + col * 16.0, 120.0 + row * 16.0, 132.0 + col * 16.0, 128.0 + row * 16.0 );
	}
}

void Game::gameDraw() {
	int tileShift = TILE_SIZE + 1;
	glPushMatrix();
	glTranslatef( 115.0, 30.0, 0.0 );
	for ( int i = 0; i < BOARD_TILES_Y; i++ ) {
		for ( int j = 0; j < BOARD_TILES_X; j++ ) {
			if ( board[i][j] == NULL ) {
				drawTile( COLOR_BLANK, colorset );
				glTranslatef( tileShift, 0.0, 0.0 );
			}
			else {
				drawTile( board[i][j]->colorid, colorset );
				glTranslatef( tileShift, 0.0, 0.0 );
			}
		}
		glTranslatef( tileShift * -BOARD_TILES_X, tileShift, 0.0 );
	}
	glPopMatrix();

	char str[32];

	glPushMatrix();
	glTranslatef( 240.0, 20.0, 0.0 );
	sys->printStr( "Score" );
	glTranslatef( 0.0, 8.0, 0.0 );
	snprintf( str, 32, "%06d", state.score );
	sys->printStr( str );
	glPopMatrix();

	glPushMatrix();
	glTranslatef( 240.0f, 70.0f, 0.0f );
	sys->printStr( "Next" );
	glTranslatef( 0.0f, 8.0f, 0.0f );
	nextBlock->draw( colorset );
	glPopMatrix();

	glPushMatrix();
	glTranslatef( 240.0, 180.0, 0.0 );
	sys->printStr( "Level" );
	glTranslatef( 16.0, 8.0, 0.0 );
	snprintf( str, 32, "%02d", state.level );
	sys->printStr( str );
	glPopMatrix();

	glPushMatrix();
	glTranslatef( 115.0, 20.0, 0.0 );
	snprintf( str, 32, "Lines-%03d", state.lines );
	sys->printStr( str );
	glPopMatrix();
	sys->checkGLError( "Draw Game" );
}

void Game::pauseDraw() {
	glPushMatrix();
	glTranslatef( 150.0, 120.0, 0.0 );
	sys->printStr( "PAUSE" );
	glPopMatrix();
}

void Game::overDraw() {
	gameDraw();

	glPushMatrix();
	glTranslatef( 124.0, 120.0, 0.0 );
	sys->printStr( "GAME OVER" );
	glPopMatrix();
}
