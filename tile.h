#ifndef TILE_H
#define TILE_H

#define TILE_SIZE 8

typedef enum {
	COLOR_BLANK,
	COLOR_ONE,
	COLOR_TWO,
	COLOR_BORDER
} colorid_t;

typedef int colorsetid_t;

typedef struct {
	unsigned char color1[3];
	unsigned char color2[3];
} colorset_t;

typedef struct {
	colorid_t colorid;
} tile_t;

void drawTile( colorid_t color, colorsetid_t colorset );

#endif //TILE_H