#include <iostream>
#include <random>
#include "block.h"
#include "tile.h"
#include "GL/glu.h"

#define TILESHIFT (TILE_SIZE + 1)

const loc_t OBLOCK_START[4] =
	{ { 4, 0 }, { 5, 0 }, { 4, 1 }, { 5, 1 } };

const loc_t JBLOCK_START[4] =
	{ { 4, 0 }, { 5, 0 }, { 6, 0 }, { 6, 1 } };

const loc_t LBLOCK_START[4] =
	{ { 5, 0 }, { 6, 0 }, { 4, 0 }, { 4, 1 } };

const loc_t TBLOCK_START[4] =
	{ { 5, 0 },{ 4, 0 },{ 6, 0 },{ 5, 1 } };

const loc_t SBLOCK_START[4] =
	{ { 5, 0 }, { 6, 0 }, { 4, 1 }, { 5, 1 } };

const loc_t ZBLOCK_START[4] =
	{ { 5, 0 }, { 5, 1 }, { 4, 0 }, { 6, 1 } };

const loc_t IBLOCK_START[4] =
	{ { 5, 0 }, { 3, 0 }, { 4, 0 }, { 6, 0 } };

block_err Block::place() {
	tile_t tile;
	tile.colorid = colorid;
	int c = 0;
	for ( int i = 0; i < 4; i++ ) {
		if ( squares[i].y >= 0 ) {
			c += game->placeTile( squares[i], tile );
		}
	}

	if ( c == 4 ) {
		return BLOCK_CANT_SPAWN;
	}

	return BLOCK_OK;
}

void Block::pickup() {
	for ( int i = 0; i < 4; i++ ) {
		if ( squares[i].y >= 0 ) {
			game->freeTile( squares[i] );
		}
	}
}

void Block::print_err( block_err err ) {
	switch ( err ) {
	case BLOCK_HITS_OCCUPIED_TILE:
		std::cout << "Block hits occupied tile!" << std::endl;
		return;
	case BLOCK_OUT_OF_BOUNDS:
		std::cout << "Block is trying to move out of bounds!" << std::endl;
		return;
	case BLOCK_OK:
		return;
	}
}

Block *newBlock( Game *game ) {
	std::random_device rd;

	switch ( rd() % 7 ) {
	case 0:
		return new OBlock( game );
	case 1:
		return new JBlock( game );
	case 2:
		return new LBlock( game );
	case 3:
		return new TBlock( game );
	case 4:
		return new SBlock( game );
	case 5:
		return new ZBlock( game );
	case 6:
		return new IBlock( game );
	}
}

block_err Block::move( int xoffset, int yoffset ) {
	std::array<loc_t,4> newpos;

	std::copy( squares.begin(), squares.end(), newpos.begin() );

	for ( int i = 0; i < 4; i++ ) {
		newpos[i].x += xoffset;
		newpos[i].y += yoffset;

		if ( newpos[i].x >= BOARD_TILES_X || newpos[i].x < 0) {
			return BLOCK_OUT_OF_BOUNDS;
		}

		if ( newpos[i].y >= BOARD_TILES_Y ) {
			return BLOCK_OUT_OF_BOUNDS;
		}

		if ( newpos[i].y >= 0 && game->hasTile( newpos[i] ) ) {
			return BLOCK_HITS_OCCUPIED_TILE;
		}
	}

	squares.swap( newpos );
	return BLOCK_OK;
}

////////////////
// O-BLOCK CODE
////////////////
OBlock::OBlock( Game *game ) : Block( game ) {
	std::copy( OBLOCK_START, OBLOCK_START + 4, squares.begin() );
	colorid = COLOR_BORDER;
}

void OBlock::draw( colorsetid_t colorset ) {
	glPushMatrix();
	glTranslatef( TILESHIFT * 1.0f, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( -TILESHIFT, TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glPopMatrix();
}

////////////////
// J-BLOCK CODE
////////////////
JBlock::JBlock( Game *game ) : Block( game ) {
	std::copy( JBLOCK_START, JBLOCK_START + 4, squares.begin() );
	rotation = 0;
	colorid = COLOR_TWO;
}

void JBlock::rotateleft() {
	switch ( rotation ) {
	case 0:
		squares[0].x += 1;
		squares[0].y += 1;
		squares[2].x -= 1;
		squares[2].y -= 1;
		squares[3].y -= 2;
		rotation = 3;
		break;
	case 1:
		squares[0].x -= 1;
		squares[0].y += 1;
		squares[2].x += 1;
		squares[2].y -= 1;
		squares[3].x += 2;
		rotation = 0;
		break;
	case 2:
		squares[0].x -= 1;
		squares[0].y -= 1;
		squares[2].x += 1;
		squares[2].y += 1;
		squares[3].y += 2;
		rotation = 1;
		break;
	case 3:
		squares[0].x += 1;
		squares[0].y -= 1;
		squares[2].x -= 1;
		squares[2].y += 1;
		squares[3].x -= 2;
		rotation = 2;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateright();
	}
}

void JBlock::rotateright() {
	switch ( rotation ) {
	case 0:		
		squares[0].x += 1;
		squares[0].y -= 1;
		squares[2].x -= 1;
		squares[2].y += 1;
		squares[3].x -= 2;
		rotation = 1;
		break;
	case 1:
		squares[0].x += 1;
		squares[0].y += 1;
		squares[2].x -= 1;
		squares[2].y -= 1;
		squares[3].y -= 2;
		rotation = 2;
		break;
	case 2:
		squares[0].x -= 1;
		squares[0].y += 1;
		squares[2].x += 1;
		squares[2].y -= 1;
		squares[3].x += 2;
		rotation = 3;
		break;
	case 3:
		squares[0].x -= 1;
		squares[0].y -= 1;
		squares[2].x += 1;
		squares[2].y += 1;
		squares[3].y += 2;
		rotation = 0;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateleft();
	}
}

void JBlock::draw( colorsetid_t colorset ) {
	glPushMatrix();
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( 0.0f, TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glPopMatrix();
}

////////////////
// L-BLOCK CODE
////////////////
LBlock::LBlock( Game *game ) : Block( game ) {
	std::copy( LBLOCK_START, LBLOCK_START + 4, squares.begin() );
	rotation = 0;
	colorid = COLOR_ONE;
}

void LBlock::rotateleft() {
	switch ( rotation ) {
	case 0:
		squares[1].x -= 1;
		squares[1].y -= 1;
		squares[2].x += 1;
		squares[2].y += 1;
		squares[3].x += 2;
		rotation = 3;
		break;
	case 1:
		squares[1].x += 1;
		squares[1].y -= 1;
		squares[2].x -= 1;
		squares[2].y += 1;
		squares[3].y += 2;
		rotation = 0;
		break;
	case 2:
		squares[1].x += 1;
		squares[1].y += 1;
		squares[2].x -= 1;
		squares[2].y -= 1;
		squares[3].x -= 2;
		rotation = 1;
		break;
	case 3:
		squares[1].x -= 1;
		squares[1].y += 1;
		squares[2].x += 1;
		squares[2].y -= 1;
		squares[3].y -= 2;
		rotation = 2;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateright();
	}
}

void LBlock::rotateright() {
	switch ( rotation ) {
	case 0:
		squares[1].x -= 1;
		squares[1].y += 1;
		squares[2].x += 1;
		squares[2].y -= 1;
		squares[3].y -= 2;
		rotation = 1;
		break;
	case 1:
		squares[1].x -= 1;
		squares[1].y -= 1;
		squares[2].x += 1;
		squares[2].y += 1;
		squares[3].x += 2;
		rotation = 2;
		break;
	case 2:
		squares[1].x += 1;
		squares[1].y -= 1;
		squares[2].x -= 1;
		squares[2].y += 1;
		squares[3].y += 2;
		rotation = 3;
		break;
	case 3:
		squares[1].x += 1;
		squares[1].y += 1;
		squares[2].x -= 1;
		squares[2].y -= 1;
		squares[3].x -= 2;
		rotation = 0;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateleft();
	}
}

void LBlock::draw( colorsetid_t colorset ) {
	glPushMatrix();
	glTranslatef( 0.0f, TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( 0.0f, -TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glPopMatrix();
}

////////////////
// T-BLOCK CODE
////////////////
TBlock::TBlock( Game *game ) : Block( game ) {
	std::copy( TBLOCK_START, TBLOCK_START + 4, squares.begin() );
	rotation = 0;
	colorid = COLOR_BORDER;
}

void TBlock::rotateleft() {
	switch ( rotation ) {
	case 0:
		squares[1].x += 1;
		squares[1].y += 1;
		squares[2].x -= 1;
		squares[2].y -= 1;
		squares[3].x += 1;
		squares[3].y -= 1;
		rotation = 3;
		break;
	case 1:
		squares[1].x -= 1;
		squares[1].y += 1;
		squares[2].x += 1;
		squares[2].y -= 1;
		squares[3].x += 1;
		squares[3].y += 1;
		rotation = 0;
		break;
	case 2:
		squares[1].x -= 1;
		squares[1].y -= 1;
		squares[2].x += 1;
		squares[2].y += 1;
		squares[3].x -= 1;
		squares[3].y += 1;
		rotation = 1;
		break;
	case 3:
		squares[1].x += 1;
		squares[1].y -= 1;
		squares[2].x -= 1;
		squares[2].y += 1;
		squares[3].x -= 1;
		squares[3].y -= 1;
		rotation = 2;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateright();
	}
}

void TBlock::rotateright() {
	switch ( rotation ) {
	case 0:
		squares[1].x += 1;
		squares[1].y -= 1;
		squares[2].x -= 1;
		squares[2].y += 1;
		squares[3].x -= 1;
		squares[3].y -= 1;
		rotation = 1;
		break;
	case 1:
		squares[1].x += 1;
		squares[1].y += 1;
		squares[2].x -= 1;
		squares[2].y -= 1;
		squares[3].x += 1;
		squares[3].y -= 1;
		rotation = 2;
		break;
	case 2:
		squares[1].x -= 1;
		squares[1].y += 1;
		squares[2].x += 1;
		squares[2].y -= 1;
		squares[3].x += 1;
		squares[3].y += 1;
		rotation = 3;
		break;
	case 3:
		squares[1].x -= 1;
		squares[1].y -= 1;
		squares[2].x += 1;
		squares[2].y += 1;
		squares[3].x -= 1;
		squares[3].y += 1;
		rotation = 0;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateleft();
	}
}

void TBlock::draw( colorsetid_t colorset ) {
	glPushMatrix();
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( -TILESHIFT, TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glPopMatrix();
}

////////////////
// S-BLOCK CODE
////////////////
SBlock::SBlock( Game *game ) : Block( game ) {
	std::copy( SBLOCK_START, SBLOCK_START + 4, squares.begin() );
	rotation = 0;
	colorid = COLOR_ONE;
}

void SBlock::rotateleft() {
	switch ( rotation ) {
	case 0:
		squares[2].x += 1;
		squares[2].y -= 2;
		squares[3].x += 1;
		rotation = 1;
		break;
	case 1:
		squares[2].x -= 1;
		squares[2].y += 2;
		squares[3].x -= 1;
		rotation = 0;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateleft();
	}
}

void SBlock::draw( colorsetid_t colorset ) {
	glPushMatrix();
	glTranslatef( 0.0f, TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( 0.0f, -TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glPopMatrix();
}

////////////////
// Z-BLOCK CODE
////////////////
ZBlock::ZBlock( Game *game ) : Block( game ) {
	std::copy( ZBLOCK_START, ZBLOCK_START + 4, squares.begin() );
	rotation = 0;
	colorid = COLOR_TWO;
}

void ZBlock::rotateleft() {
	switch ( rotation ) {
	case 0:
		squares[2].x += 2;
		squares[3].y -= 2;
		rotation = 1;
		break;
	case 1:
		squares[2].x -= 2;
		squares[3].y += 2;
		rotation = 0;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateleft();
	}
}

void ZBlock::draw( colorsetid_t colorset ) {
	glPushMatrix();
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( 0.0f, TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glPopMatrix();
}

////////////////
// I-BLOCK CODE
////////////////
IBlock::IBlock( Game *game ) : Block( game ) {
	std::copy( IBLOCK_START, IBLOCK_START + 4, squares.begin() );
	rotation = 0;
	colorid = COLOR_BORDER;
}

void IBlock::rotateleft() {
	switch ( rotation ) {
	case 0:
		squares[1].x += 2;
		squares[1].y -= 2;
		squares[2].x += 1;
		squares[2].y -= 1;
		squares[3].x -= 1;
		squares[3].y += 1;
		rotation = 1;
		break;
	case 1:
		squares[1].x -= 2;
		squares[1].y += 2;
		squares[2].x -= 1;
		squares[2].y += 1;
		squares[3].x += 1;
		squares[3].y -= 1;
		rotation = 0;
		break;
	default:
		;
	}

	if ( move( 0, 0 ) != BLOCK_OK ) {
		rotateleft();
	}
}

void IBlock::draw( colorsetid_t colorset ) {
	glPushMatrix();
	glTranslatef( 0.0f, TILESHIFT, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glTranslatef( TILESHIFT, 0.0f, 0.0f );
	drawTile( colorid, colorset );
	glPopMatrix();
}
