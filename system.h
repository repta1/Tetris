#include "SDL2/SDL.h"

#ifndef SYSTEM_H
#define SYSTEM_H

typedef enum {
	SYS_OK = 0,
	SYS_ERR_INIT_SDL,
	SYS_ERR_CREAT_WIN,
	SYS_ERR_CREAT_GLCON
} syserr_t;

typedef struct {
	bool upKey;
	bool downKey;
	bool leftKey;
	bool rightKey;
} keyState_t;

class Game;

class System {
public:
	System();
	~System();
	void mainLoop();
	syserr_t getErr() { return err; }
	keyState_t getKeys() { return keyboard; };
	void printStr( const char *s );
	void checkGLError( const char *s );
	void quit();

private:
	syserr_t err;
	SDL_Window *win;
	SDL_GLContext glcon;
	Uint32 sdlTicks;
	Uint32 ticks;
	keyState_t keyboard;
	Game *game;
	bool running;

	void doWindow( SDL_WindowEvent *evn );
	void doInput( SDL_Event *evn );

};

#endif // SYSTEM_H
