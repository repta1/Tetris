#include <iostream>

#define SDL_MAIN_HANDLED
#include "SDL2/SDL.h"
#include "system.h"

int main( int argc, char *argv[] ) {
	SDL_SetMainReady();

	System *sys = new System;

	if ( sys->getErr() == SYS_OK ) {
		sys->mainLoop();
	}

	delete sys;
	return 0;
}
