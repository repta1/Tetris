#include <array>
#include "SDL2/SDL.h"
#include "tile.h"
#include "system.h"

#ifndef GAME_H
#define GAME_H

#define BOARD_TILES_X 10
#define BOARD_TILES_Y 20

typedef enum {
	MODE_TITLE,
	MODE_SELECT,
	MODE_GAME,
	MODE_PAUSE,
	MODE_OVER
} gameMode_t;

typedef struct {
	signed char x, y;
} loc_t;

typedef struct {
	Uint32 score;
	Uint16 lines;
	unsigned char level;
	std::array<unsigned char, 7> blockcounts;
} gameState_t;

class Block;

class Game {
public:
	Game( System *sys );
	void runtic();
	void draw();
	void doInput(SDL_Event *evn);
	// Board functions:
	bool hasTile( loc_t loc );
	int placeTile( loc_t loc, tile_t tile );
	void freeTile( loc_t loc );
	void checkRows();

private:
	System *sys;
	gameMode_t mode;
	unsigned int tic;
	unsigned int blockTic;
	colorsetid_t colorset;
	Block *currentBlock;
	Block *nextBlock;
	std::array<std::array<tile_t *, BOARD_TILES_X>, BOARD_TILES_Y> board;
	gameState_t state;

	void init();

	// Mode-dependent functions
	void titleTic();
	void selectTic();
	void gameTic();

	void titleInput( SDL_Event *evn );
	void selectInput( SDL_Event *evn );
	void gameInput( SDL_Event *evn );
	void pauseInput( SDL_Event *evn );
	void overInput( SDL_Event *evn );

	void titleDraw();
	void selectDraw();
	void gameDraw();
	void pauseDraw();
	void overDraw();
};

#endif //GAME_H
