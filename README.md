A quick Tetris clone made over a summer.

Lots of improvements can be made. I'm pretty sure there's a memory leak in the way tiles are stored on the board, and there's lots of redundant drawing on the screen.